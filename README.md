# daverona/mantisbt <!-- alpine docker mantisbt docker alpine -->

[![pipeline status](https://gitlab.com/daverona/docker/mantisbt/badges/master/pipeline.svg)](https://gitlab.com/daverona/docker/mantisbt/-/commits/master)

This is a repository for Docker images of [Mantis Bug Tracker](https://www.mantisbt.org/).

* GitLab repository: [https://gitlab.com/daverona/docker/mantisbt](https://gitlab.com/daverona/docker/mantisbt)
* Docker registry: [https://hub.docker.com/r/daverona/mantisbt](https://hub.docker.com/r/daverona/mantisbt)
* Available releases: [https://gitlab.com/daverona/docker/mantisbt/-/releases](https://gitlab.com/daverona/docker/mantisbt/-/releases)

## Quick Start

Create a database first. If you use MariaDB or variants:

```sql
CREATE USER 'mantis'@'%';
SET PASSWORD FOR 'mantis'@'%' = PASSWORD('secret');
DROP DATABASE IF EXISTS mantis;
CREATE DATABASE mantis DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON mantis.* TO 'mantis'@'%';
FLUSH PRIVILEGES;
```

In this example `mantis`, `secret`, and `mantis` are used for the username, password, and database respectively.
These values are asked when MantisBT runs for the first time. If you use PostgreSQL, that's fine. Create a schema.

Run the container:

```bash
docker container run --rm \
  --detach \
  --publish 80:80 \
  --name mantisbt \
  daverona/mantisbt
```

If your DBMS is a container on Docker network, say `database`, add option `--network database`.

Visit [http://localhost](http://localhost) and continue to configure MantisBT.
The default username and password are `administrator` and `root` respectively.

Once done, copy the configuration file `config_inc.php`from the container to your host and stop the container:

```bash
docker container exec mantisbt cat /app/config/config_inc.php > config_inc.php
docker container stop mantisbt
```

You may want to review and customize more `config_inc.php`. For more information read [Admin Guide](https://www.mantisbt.org/docs/master/en-US/Admin_Guide/html-desktop/).

Start the container *with* `config_inc.php` mounted to `/app/config`:

```bash
docker container run --rm \
  --detach \
  --volume $PWD/config_inc.php:/app/config/config_inc.php:ro \
  --publish 80:80 \
  --name mantisbt \
  daverona/mantisbt
```

## Advanced Usages

To log mantisbt output to stdout, add to `config_inc.php`:

```php
$g_log_destination = 'php://stdout';
```

To log mantisbt output to a file, add to `config_inc.php`:

```php
$g_log_destination = 'file://var/log/mantis/mantis.log';
```

Note that the directory containing mantisbt log file needs be owned 
by web server and `/var/log/mantis` is prepared for this purpose.

## References

* MantisBT documentation: [https://www.mantisbt.org/](https://www.mantisbt.org/)
* Admin Guide: [https://www.mantisbt.org/docs/master/en-US/Admin\_Guide/html-desktop/](https://www.mantisbt.org/docs/master/en-US/Admin_Guide/html-desktop/)
* MantisBT repository: [https://github.com/mantisbt/mantisbt](https://github.com/mantisbt/mantisbt)
