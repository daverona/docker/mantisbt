FROM alpine:3.10

ENV LANG=C.UTF-8

# Install apache, composer, and php
RUN apk add --no-cache \
    apache2 \
    composer \
    php7 \
    php7-apache2 \
    tzdata \
  # Update composer
  && composer self-update \
  # Configure apache
  && sed -i -e "s|^LoadModule info_module |#LoadModule info_module |" \
    -e "s|^LoadModule status_module |#LoadModule status_module |" \
    -e "s|^ServerSignature .*$|ServerSignature Off|" \
    -e "s|^ServerTokens .*$|ServerTokens Prod|" \
    -e "s|^#LoadModule unique_id_module |LoadModule unique_id_module |" \
    # Enable rewrite module
    -e "s|^#LoadModule rewrite_module |LoadModule rewrite_module |" \
    -e "s|LogLevel .*$|LogLevel warn|" /etc/apache2/httpd.conf \
  # @see https://github.com/docker-library/php/blob/master/apache-Dockerfile-block-1
  && ln -sfT /dev/stderr "/var/log/apache2/error.log" \
  && ln -sfT /dev/stdout "/var/log/apache2/access.log"

# Install PHP extension dependencies
RUN apk add --no-cache \
    # @see https://www.mantisbt.org/docs/master/en-US/Admin_Guide/html/admin.install.requirements.software.html
    php7-ctype \
    php7-curl \
    php7-fileinfo \
    php7-gd \
    php7-ldap \
    php7-mbstring \
    php7-mysqli \
    php7-pgsql \
    php7-session \
    php7-simplexml \
    php7-soap \
    php7-tokenizer \
    # XmlImportExport (Import/Export issues) plugin requires these two:
    # @see https://github.com/mantisbt/mantisbt/tree/release-2.24.1/plugins/XmlImportExport
    php7-xmlreader \
    php7-xmlwriter

ARG MANTISBT_VERSION=release-2.24.3

# Install mantisbt
RUN { \
    echo 'DocumentRoot "/app"'; \
    echo '<Directory "/app">'; \
    echo -e '\tOptions -Indexes'; \
    echo -e '\tAllowOverride All'; \
    echo -e '\tRequire all granted'; \
    echo '</Directory>'; \
  } | tee /etc/apache2/conf.d/mantisbt.conf \
  # Make log directory /var/log/mantis for mantisbt
  # @see https://www.mantisbt.org/docs/master/en-US/Admin_Guide/html/admin.config.logging.html
  && mkdir -p /var/log/mantis \
  && chown -R apache:apache /var/log/mantis \
  # Make /app as webroot for mantisbt
  && mkdir -p /app && cd /app \
  # Fetch mantisbt
  && wget --quiet --output-document=- "https://github.com/mantisbt/mantisbt/archive/${MANTISBT_VERSION}.tar.gz" \
    | tar -zxvf - --strip-components=1 \
  # Remove files and directories for development
  && rm -rf doc docbook scripts tests && (rm -rf .* > /dev/null 2>&1 || true) \
  # Install PHP package dependencise for production
  && apk add --no-cache --virtual=fetch-deps git \
  && composer install --no-cache --no-dev --optimize-autoloader \
  && find /app -type d -name ".git" -exec rm -rf {} + \
  && apk del --no-cache fetch-deps \
  && rm -rf /root/.composer \
  # Give to apache mantisbt webroot ownership
  && chown -R apache:apache /app \
  # Secure admin directory to another place, out of webroot
  && tar -zcvf /mantisbt-admin.tgz admin && rm -rf admin

# Move config, core, lang, and library directory out of webroot
# @see http://localhost/admin/check/index.php
# @see https://www.mantisbt.org/docs/master/en-US/Admin_Guide/html/admin.config.path.html
#
# Note that this takes quiet an effort due to hard-coded paths everywhere.
# DO NOT DO THIS.
# @see https://www.mantisbt.org/bugs/view.php?id=21584
#
# ENV MANTIS_CONFIG_FOLDER=/opt/app/config/
# RUN MANTIS_SAFE_FOLDER=$(dirname $MANTIS_CONFIG_FOLDER) \
#   && mkdir -p "$MANTIS_SAFE_FOLDER" \
#   && mv /app/config /app/core /app/lang /app/library "$MANTIS_SAFE_FOLDER" \
#   && sed -i -e "s|^\$g_core_path = \$g_absolute_path|\$g_core_path = '$MANTIS_SAFE_FOLDER/'|" \
#      -e "s|^\$g_language_path = \$g_absolute_path|\$g_language_path = '$MANTIS_SAFE_FOLDER/'|" \
#      -e "s|^\$g_library_path = \$g_absolute_path|\$g_library_path = '$MANTIS_SAFE_FOLDER/'|" /app/config_defaults_inc.php \
#   && sed -i -e "s|^require_once( dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'core'|require_once( '$MANTIS_SAFE_FOLDER/core'|" /app/core.php \
#   && chown -R apache:apache "$MANTIS_SAFE_FOLDER"

# Configure miscellanea
COPY docker-entrypoint.sh /
RUN chmod a+x /docker-entrypoint.sh
EXPOSE 80
WORKDIR /app
STOPSIGNAL SIGWINCH

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["httpd", "-DFOREGROUND"]
