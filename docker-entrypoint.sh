#!/bin/ash
set -e

[ ! -z "$TZ" ] && sed -i "s|^;date.timezone =.*$|date.timezone = $TZ|" /etc/php7/php.ini

if [ "httpd" == "$(basename $1)" ]; then
  # Apache gets grumpy about PID files pre-existing
  # @see https://github.com/docker-library/httpd/blob/master/2.4/alpine/httpd-foreground
  rm -rf /var/run/apache2/httpd.pid

  # Make sure apache has ownership of /var/log/mantis
  # @see https://www.mantisbt.org/docs/master/en-US/Admin_Guide/html/admin.config.logging.html
  chown -R apache:apache /var/log/mantis

  # If /app/config/config_inc.php does not exist, assume that mantisbt runs for the first time.
  if [ ! -f "/app/config/config_inc.php" ]; then
    echo "Preparing to configure MantisBT..."
    rm -rf admin 
    tar -zxvf /mantisbt-admin.tgz >/dev/null 2>&1
    chown -R apache:apache admin
  fi
fi

exec "$@"
